FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

# we really want node 10. with node 14, various things crash. node 10.24.0?
ARG NODE_VERSION=10.24.1
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION

ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

# this is based off https://github.com/cloudron-io/mailtrain
ARG VERSION=86748f62a09d95e0299617f2b78ae8477b8a4be6

RUN curl -L https://github.com/cloudron-io/mailtrain/archive/${VERSION}.tar.gz | tar -xz --strip-components 1 -f -
RUN npm install --production && npm install passport-ldapjs tomlify-j0.4
COPY toml-override.js /app/code/toml-override.js

# Certain directories needs to be backed up (see #13)
RUN rm -rf /app/code/public/mosaico/uploads && ln -s /app/data/public/mosaico/uploads /app/code/public/mosaico/uploads && \
    rm -rf /app/code/public/grapejs/uploads && ln -s /app/data/public/grapejs/uploads /app/code/public/grapejs/uploads && \
    rm -rf /app/code/protected/reports && ln -s /app/data/protected/reports /app/code/protected/reports

RUN ln -s /run/mailtrain/production.toml /app/code/config/production.toml && \
    ln -s /run/mailtrain/production-reports.toml /app/code/workers/reports/config/production.toml

COPY cloudron-setup.js production.toml.template production-reports.toml.template start.sh /app/code/
COPY index.hbs layout.hbs /app/code/views/

CMD [ "/app/code/start.sh" ]
