#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

var assert = require('assert'),
    execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    var browser;
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT)
            .then(function () {
                return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
            });
    }

    function login(done) {
        browser.get('https://' + app.fqdn + '/users/login').then(function () {
            return waitForElement(By.id('usernameMain'));
        }).then(function () {
            return browser.findElement(By.id('usernameMain')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.id('passwordMain')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.tagName('form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//*[text()[contains(., "' + username + '")]]'));
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(By.xpath('//*[text()[contains(., "' + username + '")]]'));
        }).then(function () {
            return browser.findElement(By.xpath('//*[text()[contains(., "' + username + '")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//*[text()[contains(., "Log out")]]'));
        }).then(function () {
            return browser.findElement(By.xpath('//*[text()[contains(., "Log out")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//*[text()[contains(., "Sign in")]]'));
        }).then(function () {
            done();
        });
    }

    function checkSettings(done) {
        browser.get('https://' + app.fqdn + '/settings').then(function () {
            return browser.findElement(By.id('service-url')).getAttribute('value');
        }).then(function (value) {
            expect(value).to.be('https://' + app.fqdn + '/');
            return browser.findElement(By.id('smtp-hostname')).getAttribute('value');
        }).then(function (value) {
            expect(value).to.be('mail');
            done();
        });
    }

    function checkMailerConfig(done) {
        browser.get('https://' + app.fqdn + '/settings');

        var button = browser.findElement(By.id('verify-button'));

        browser.executeScript('arguments[0].scrollIntoView(false)', button).then(function () {
            return button.click();
        }).then(function () {
            console.log('The test will dismiss the alert dialog after 5 seconds');
            return browser.sleep(5000);
        }).then(function () {
            var alert = browser.switchTo().alert();
            alert.getText().then(function (text) {
                expect(text).to.be('Mailer settings verified, ready to send some mail!');

                alert.accept().then(function () { done(); });
            });
        });
    }

    function createTemplate(done) {
        browser.get('https://' + app.fqdn + '/templates/create').then(function () {
            return browser.findElement(By.id('template-name')).sendKeys('custommosaico');
        }).then(function () {
            return browser.findElement(By.css('#editor_name>option[value="mosaico"]')).click();
        }).then(function () {
            return browser.findElement(By.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            done();
        });
    }

    function uploadMosaicoImage(done) {
        var openMosaico;
        var imageGallery;

        browser.get('https://' + app.fqdn + '/templates/edit/1').then(function () {
            return browser.wait(until.elementLocated(By.xpath('//a[text()="Open Mosaico"]')));
        }).then(function () {
            openMosaico = browser.findElement(By.xpath('//a[text()="Open Mosaico"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', openMosaico);
        }).then(function () {
            return openMosaico.click();
        }).then(function () {
            console.log('sleeping for 5secs for page to load');
            return browser.sleep(5000);
        }).then(function () {
            return browser.switchTo().frame('editor-frame');
        }).then(function () {
            imageGallery = browser.findElement(By.xpath('//label[@title="Show image gallery"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', imageGallery);
        }).then(function () {
            return imageGallery.click();
        }).then(function () {
            console.log('sleeping for animation');
            return browser.sleep(5000);
        }).then(function () {
            return browser.findElement(By.xpath('//input[@class="fileupload"]')).sendKeys(path.resolve(__dirname + '/../logo.png'));
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//img[@title="Drag this image and drop it on any template image placeholder"]')), TEST_TIMEOUT);
        }).then(function () {
            browser.switchTo().defaultContent();
            done();
        });
    }

    function checkGallery(done) {
        var openMosaico;
        browser.get('https://' + app.fqdn + '/templates/edit/1').then(function () {
            return browser.wait(until.elementLocated(By.xpath('//a[text()="Open Mosaico"]')));
        }).then(function () {
            openMosaico = browser.findElement(By.xpath('//a[text()="Open Mosaico"]'));
            return browser.executeScript('arguments[0].scrollIntoView(false)', openMosaico);
        }).then(function () {
            return openMosaico.click();
        }).then(function () {
            return browser.sleep(10000);
        }).then(function () {
            return browser.switchTo().frame('editor-frame');
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('//label[@title="Show image gallery"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(By.xpath('//label[@title="Show image gallery"]')).click();
        }).then(function () {
            return browser.sleep(10000); // let the animation finish
        }).then(function () {
            return browser.findElement(By.xpath('//a[@title="Remote gallery"]')).click();
        }).then(function () {
            return browser.sleep(10000); // let the images load
        }).then(function () {
            var img = browser.findElement(By.xpath('//img[@title="Drag this image and drop it on any template image placeholder"]'));
            return browser.executeScript('return arguments[0].complete && arguments[0].naturalWidth', img);
        }).then(function (imageWidth) {
            assert(imageWidth === 90);
            browser.switchTo().defaultContent();
            done();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('setup settings correctly', checkSettings);
    it('check mailer config', checkMailerConfig);
    it('can create template', createTemplate);
    it('can upload mosaico image', uploadMosaicoImage);
    it('can check gallery', checkGallery);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can check gallery', checkGallery);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();

        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);

        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('setup settings correctly', checkSettings);
    it('check mailer config', checkMailerConfig);
    it('can check gallery', checkGallery);

    it('can logout', logout);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.mailtrain.cloudronapp --location ' + LOCATION, EXEC_ARGS);

        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can login', login);
    it('check mailer config', checkMailerConfig);
    it('can create template', createTemplate);
    it('can upload mosaico image', uploadMosaicoImage);
    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });
    it('can login', login);
    it('setup settings correctly', checkSettings);
    it('check mailer config', checkMailerConfig);
    it('can check gallery', checkGallery);
    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

});
