#!/bin/bash

set -eu

export NODE_ENV=production

# https://github.com/Mailtrain-org/mailtrain/issues/250
mkdir -p /run/mailtrain /app/data/public/mosaico/uploads /app/data/public/grapejs/uploads /app/data/protected/reports

sed -e "s!##MYSQL_HOST##!${CLOUDRON_MYSQL_HOST}!" \
    -e "s!##MYSQL_PORT##!${CLOUDRON_MYSQL_PORT}!" \
    -e "s!##MYSQL_USERNAME##!${CLOUDRON_MYSQL_USERNAME}!" \
    -e "s!##MYSQL_PASSWORD##!${CLOUDRON_MYSQL_PASSWORD}!" \
    -e "s!##MYSQL_DATABASE##!${CLOUDRON_MYSQL_DATABASE}!" \
    -e "s!##REDIS_HOST##!${CLOUDRON_REDIS_HOST}!" \
    -e "s!##REDIS_PORT##!${CLOUDRON_REDIS_PORT}!" \
    -e "s!##REDIS_PASSWORD##!${CLOUDRON_REDIS_PASSWORD}!" \
    -e "s!##LDAP_SERVER##!${CLOUDRON_LDAP_SERVER}!" \
    -e "s!##LDAP_PORT##!${CLOUDRON_LDAP_PORT}!" \
    -e "s!##LDAP_USERS_BASE_DN##!${CLOUDRON_LDAP_USERS_BASE_DN}!" \
    -e "s!##WEBADMIN_ORIGIN##!${CLOUDRON_WEBADMIN_ORIGIN}!" \
    /app/code/production.toml.template > /run/mailtrain/production.toml

sed -e "s!##MYSQL_HOST##!${CLOUDRON_MYSQL_HOST}!" \
    -e "s!##MYSQL_PORT##!${CLOUDRON_MYSQL_PORT}!" \
    -e "s!##MYSQL_USERNAME##!${CLOUDRON_MYSQL_USERNAME}!" \
    -e "s!##MYSQL_PASSWORD##!${CLOUDRON_MYSQL_PASSWORD}!" \
    -e "s!##MYSQL_DATABASE##!${CLOUDRON_MYSQL_DATABASE}!" \
    /app/code/production-reports.toml.template > /run/mailtrain/production-reports.toml

if [[ ! -f /app/data/production.toml ]]; then
    echo "# Add additional customizations in this file" > /app/data/production.toml
fi

if [[ ! -f /app/data/production-reports.toml ]]; then
    echo "# Add additional customizations for reports in this file" > /app/data/production-reports.toml
fi

chown -R cloudron:cloudron /app/data /run/mailtrain

# merge user toml file (toml does not allow key re-declaration)
/usr/local/bin/gosu cloudron:cloudron node /app/code/toml-override.js /run/mailtrain/production.toml /app/data/production.toml
/usr/local/bin/gosu cloudron:cloudron node /app/code/toml-override.js /run/mailtrain/production-reports.toml /app/data/production-reports.toml

cd /app/code

/usr/local/bin/gosu cloudron:cloudron node cloudron-setup.js

# delete the auto-generated admin user. note one cannot login with this anyway when ldap is enabled
# mailtrain has only admin per role (no such thing as normal user)
mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "DELETE FROM users WHERE username='admin'"

exec /usr/local/bin/gosu cloudron:cloudron npm start

