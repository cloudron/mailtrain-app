This app packages mailtrain version <upstream>1.24.1</upstream>

Mailtrain is a newsletter and mail campaign application.

## Features

### List management

Mailtrain allows you to easily manage even very large lists. Million subscribers? Not a problem. You can add subscribers manually, through the API or import from a CSV file. All lists come with support for custom fields and merge tags as well.

### Custom fields

Text fields, numbers, drop downs or checkboxes, Mailtrain has them all. Every custom field can be included in the generated newsletters through merge tags.

### List segmentation

Send messages only to list subscribers that match predefined segmentation rules. No need to create separate lists with small differences.

### RSS Campaigns

Setup Mailtrain to track RSS feeds and if a new entry is detected in a feed then Mailtrain auto-generates a new campaign using entry data as message contents and sends it to selected subscribers.

### GPG Encryption

If a list has a custom field for a GPG Public Key set then subscribers can upload their GPG public key to receive encrypted messages from the list.

### Click stats

After a campaign is sent, check individual click statistics for every link included in the message.

### Trigger based automation

Define automation triggers to send specific messages when a user activates the trigger.
