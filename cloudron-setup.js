'use strict';

/**
 * Module dependencies.
 */

let config = require('config');
let log = require('npmlog');
let dbcheck = require('./lib/dbcheck');
let settings = require('./lib/models/settings');

log.level = config.log.level;

// Check if database needs upgrading before starting the server
dbcheck(err => {
    if (err) {
        log.error('DB', err.message || err);
        return process.exit(1);
    }

    log.info('DB update or init successful');

    // update settings
    settings.get('smtp_hostname', (err, res) => {
        if (err) {
            log.error('DB', err.message || err);
            return process.exit(1);
        }

        // taken from routes/settings.js
        let keys = [];
        let values = [];

        // only update the values if not exist or the current server is set to the cloudron mail server ('mail')
        // "localhost" is the default unset value, so we want to provision it
        // mailtrain switched the default to smtp-pulse.com (9946f70992d14cc378b8717f28bf972b45f91d86)
        if (!res || res === 'localhost' || res === 'smtp-pulse.com' || res === process.env.CLOUDRON_MAIL_SMTP_SERVER) {
            log.info('DB configuring for Cloudron mail');

            keys.push('smtp_hostname');
            values.push(process.env.CLOUDRON_MAIL_SMTP_SERVER);
            keys.push('smtp_port');
            values.push(process.env.CLOUDRON_MAIL_SMTP_PORT);
            keys.push('smtp_encryption');
            values.push('NONE');
            keys.push('smtp_user');
            values.push(process.env.CLOUDRON_MAIL_SMTP_USERNAME);
            keys.push('smtp_pass');
            values.push(process.env.CLOUDRON_MAIL_SMTP_PASSWORD);
        } else {
            log.info('DB configured for external mail');
        }

        keys.push('service_url');
        values.push(process.env.CLOUDRON_APP_ORIGIN + '/');
        keys.push('default_address');
        values.push(process.env.CLOUDRON_MAIL_FROM);
        keys.push('admin_email');
        values.push(process.env.CLOUDRON_MAIL_FROM);

        let i = 0;
        let updateSettings = () => {
            if (i >= keys.length) {
                log.info('Settings updated');
                return process.exit(0);
            }

            let key = keys[i];
            let value = values[i];
            i++;

            settings.set(key, value, err => {
                if (err) {
                    req.flash('danger', err && err.message || err);
                    return res.redirect('/settings');
                }
                updateSettings();
            });
        };

        updateSettings();
    });
});
